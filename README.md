# Cephiroth

## Production backend

In order to connect local UI to production backend execute the following command. Details in `src/setupProxy.js`
```bash
SERVER=https://cephiroth.com yarn start
```

## USING YARN (Recommend)

- yarn install
- yarn start

## USING NPM

- npm i OR npm i --legacy-peer-deps
- npm start


# About zip

**This package will include directories:**

### 1.cra folder

- Using react script & react router.

### 2.nextjs folder

- Using for Next.Js

### 3.simple folder

- To remove unnecessary components. This is a simplified version ([https://simple.minimals.cc/](https://simple.minimals.cc/))
- Good to start a new project. You can copy components from the full version.
- Make sure to install the dependencies exactly as compared to the full version

**NOTE:**

**1 - Recommended environment:**

- node js 16.x
- npm 6+

**2 - After downloading and extracting please do not delete any files.**

## FOR CRA (REACT CREATE APP) :

**Using Yarn (Recommend)**

- yarn install
- yarn start

**Using Npm**

- npm i OR npm i --legacy-peer-deps
- npm start

## FOR NEXT.JS :

**Using Yarn (Recommend)**

- yarn install
- yarn dev

**Using Npm**

- npm i OR npm i --legacy-peer-deps
- npm run dev

[Learn more:](https://docs.minimals.cc/quick-start)
