import { createSlice, Dispatch } from '@reduxjs/toolkit';
import axios from '../../utils/axios';

export type IArticle = {
  message: string;
};

export type IArticleState = {
  isLoading: boolean;
  error: Error | string | null;
  articles: IArticle[];
};

const initialState: IArticleState = {
  isLoading: false,
  error: null,
  articles: [],
};

const slice = createSlice({
  name: 'article',
  initialState,
  reducers: {
    startLoading(state) {
      state.isLoading = true;
    },
    hasError(state, action) {
      state.isLoading = false;
      state.error = action.payload;
    },
    getArticlesSuccess(state, action) {
      state.isLoading = false;
      state.articles = action.payload;
    },
  },
});

export default slice.reducer;

export function getArticles() {
  return async (dispatch: Dispatch) => {
    dispatch(slice.actions.startLoading);
    try {
      const response = await axios.get('/articles');
      dispatch(slice.actions.getArticlesSuccess(response.data));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    }
  };
}
