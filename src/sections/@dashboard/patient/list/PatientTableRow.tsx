import { useState } from 'react';
import {
  Avatar,
  Button,
  Checkbox,
  IconButton,
  Link,
  MenuItem,
  Stack,
  TableCell,
  TableRow,
  Typography,
} from '@mui/material';
import { Link as RouterLink, LinkProps as RouterLinkProps, MemoryRouter } from 'react-router-dom';
import { ShortPatient } from '../../../../service/types';
import MenuPopover from '../../../../components/menu-popover';
import Iconify from '../../../../components/iconify';
import ConfirmDialog from '../../../../components/confirm-dialog';
import { CustomAvatar } from '../../../../components/custom-avatar';
import { PATH_DASHBOARD } from '../../../../routes/paths';
import { patientFullName, shortUUID } from '../../../../utils/patient';

interface PatientTableRowProps {
  patient: ShortPatient;
  selected: boolean;
  onViewRow: VoidFunction;
  onEditRow: VoidFunction;
  onSelectRow: VoidFunction;
  onDeleteRow: VoidFunction;
}

export default function PatientTableRow({
  patient,
  selected,
  onEditRow,
  onViewRow,
  onSelectRow,
  onDeleteRow,
}: PatientTableRowProps) {
  const { firstName, lastName } = patient;
  const [openConfirm, setOpenConfirm] = useState(false);

  const [openPopover, setOpenPopover] = useState<HTMLElement | null>(null);

  const handleOpenConfirm = () => {
    setOpenConfirm(true);
  };

  const handleCloseConfirm = () => {
    setOpenConfirm(false);
  };

  const handleOpenPopover = (event: React.MouseEvent<HTMLElement>) => {
    setOpenPopover(event.currentTarget);
  };

  const handleClosePopover = () => {
    setOpenPopover(null);
  };
  return (
    <>
      <TableRow hover selected={selected}>
        <TableCell padding="checkbox">
          <Checkbox checked={selected} onClick={onSelectRow} />
        </TableCell>

        <TableCell align="left">
          <Stack direction="row" alignItems="center" spacing={2}>
            <CustomAvatar name={patientFullName(patient)} />

            <div>
              <Typography variant="subtitle2" noWrap>
                {firstName}
              </Typography>

              <Link
                noWrap
                variant="body2"
                onClick={onViewRow}
                component={RouterLink}
                to={PATH_DASHBOARD.patient.patient(patient.id)}
                sx={{ color: 'text.disabled', cursor: 'pointer' }}
              >
                {shortUUID(patient.id)}
              </Link>
            </div>
          </Stack>
        </TableCell>

        <TableCell align="left">{lastName}</TableCell>

        <TableCell align="right">
          <IconButton color={openPopover ? 'inherit' : 'default'} onClick={handleOpenPopover}>
            <Iconify icon="eva:more-vertical-fill" />
          </IconButton>
        </TableCell>
      </TableRow>

      <MenuPopover
        open={openPopover}
        onClose={handleClosePopover}
        arrow="right-top"
        sx={{ width: 140 }}
      >
        <MenuItem
          onClick={() => {
            handleOpenConfirm();
            handleClosePopover();
          }}
          sx={{ color: 'error.main' }}
        >
          <Iconify icon="eva:trash-2-outline" />
          Delete
        </MenuItem>

        <MenuItem
          onClick={() => {
            onEditRow();
            handleClosePopover();
          }}
        >
          <Iconify icon="eva:edit-fill" />
          Edit
        </MenuItem>
      </MenuPopover>

      <ConfirmDialog
        open={openConfirm}
        onClose={handleCloseConfirm}
        title="Delete"
        content="Are you sure want to delete?"
        action={
          <Button variant="contained" color="error" onClick={onDeleteRow}>
            Delete
          </Button>
        }
      />
    </>
  );
}
