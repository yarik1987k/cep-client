import React, { useEffect, useMemo } from 'react';
import * as Yup from 'yup';
import { useNavigate } from 'react-router-dom';
import { yupResolver } from '@hookform/resolvers/yup';
import { useSnackbar } from 'notistack';
import { useForm } from 'react-hook-form';
import { Box, Card, Grid, Stack } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import FormProvider, { RHFTextField } from '../../../components/hook-form';
import { ShortPatient } from '../../../service/types';

interface PatientNewEditFormProps {
  isEdit?: boolean;
  currentPatient?: ShortPatient;
  submit: (patient: ShortPatient) => Promise<unknown>;
}

const NewPatientSchema = Yup.object().shape({
  firstName: Yup.string(),
  lastName: Yup.string(),
  description: Yup.string(),
});

export const PatientNewEditForm: React.FunctionComponent<PatientNewEditFormProps> = ({
  isEdit = false,
  currentPatient,
  submit,
}) => {
  const defaultValues = useMemo(
    () => ({
      firstName: currentPatient?.firstName || '',
      lastName: currentPatient?.lastName || '',
      description: currentPatient?.description || '',
    }),
    [currentPatient]
  );

  const methods = useForm<ShortPatient>({
    resolver: yupResolver(NewPatientSchema),
    defaultValues,
  });

  const {
    reset,
    watch,
    control,
    setValue,
    handleSubmit,
    formState: { isSubmitting },
  } = methods;

  const values = watch();

  useEffect(() => {
    if (isEdit && currentPatient) {
      reset(defaultValues);
    }
    if (!isEdit) {
      reset(defaultValues);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isEdit, currentPatient]);

  const onSubmit = async (data: ShortPatient) => submit(data);

  return (
    <FormProvider methods={methods} onSubmit={handleSubmit(onSubmit)}>
      <Grid container spacing={3}>
        <Grid item xs={12} md={12}>
          {/* <Card sx={{ p: 3 }}> */}
          <Box
            rowGap={3}
            columnGap={2}
            display="grid"
            gridTemplateColumns={{
              xs: 'repeat(1, 1fr)',
              sm: 'repeat(2, 1fr)',
            }}
          >
            <RHFTextField name="firstName" label="First name" />
            <RHFTextField name="lastName" label="Last name" />
          </Box>
          {/* </Card> */}
        </Grid>
        <Grid item xs={12} md={12}>
          <RHFTextField name="description" label="Description" multiline />
          <Stack alignItems="flex-end" sx={{ mt: 3 }}>
            <LoadingButton type="submit" variant="contained" loading={isSubmitting}>
              {!isEdit ? 'Create Patient' : 'Save Changes'}
            </LoadingButton>
          </Stack>
        </Grid>
      </Grid>
    </FormProvider>
  );
};
