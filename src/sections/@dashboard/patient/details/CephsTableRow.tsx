import React, { useState } from 'react';
import { Checkbox, Link, Stack, TableCell, TableRow } from '@mui/material';
import { Link as RouterLink } from 'react-router-dom';
import { parseISO } from 'date-fns';
import { Ceph } from '../../../../service/types';
import { shortUUID } from '../../../../utils/patient';
import { fDateTime } from '../../../../utils/formatTime';
import { PATH_DASHBOARD } from '../../../../routes/paths';

interface CephsTableRowProps {
  ceph: Ceph;
}

export const CephsTableRow: React.FunctionComponent<CephsTableRowProps> = ({ ceph }) => {
  const [openDetails, setOpenDetails] = useState(false);

  return (
    <TableRow
      sx={{
        borderRadius: 1,
        '& .MuiTableCell-root': {
          bgcolor: 'background.default',
        },
        ...(openDetails && {
          '& .MuiTableCell-root': {
            color: 'text.primary',
            typography: 'subtitle2',
            bgcolor: 'background.default',
          },
        }),
      }}
    >
      <TableCell
        padding="checkbox"
        sx={{
          borderTopLeftRadius: 8,
          borderBottomLeftRadius: 8,
        }}
      >
        <Checkbox
          checked={false}
          onDoubleClick={() => console.log('ON DOUBLE CLICK')}
          onClick={() => null}
        />
      </TableCell>
      <TableCell onClick={() => null}>
        <Stack direction="row" alignItems="center" spacing={2}>
          {/* <Typography noWrap variant="inherit" sx={{ maxWidth: 360, cursor: 'pointer' }}> */}
          {/*  {shortUUID(ceph.id)} */}
          {/* </Typography> */}
          <Link
            noWrap
            variant="body2"
            component={RouterLink}
            to={PATH_DASHBOARD.ceph.ceph(ceph.id)}
            sx={{ color: 'text.disabled', cursor: 'pointer' }}
          >
            {shortUUID(ceph.id)}
          </Link>
        </Stack>
      </TableCell>
      <TableCell>{fDateTime(parseISO(ceph.createdAt))}</TableCell>
      <TableCell align="right" />
    </TableRow>
  );
};
