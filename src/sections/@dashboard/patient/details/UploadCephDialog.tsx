import React, { useCallback, useEffect, useState } from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogProps,
  DialogTitle,
  LinearProgress,
} from '@mui/material';
import { Upload } from '../../../../components/upload';

interface UploadCephDialogProps extends DialogProps {
  open: boolean;
  onClose: VoidFunction;
  loading?: boolean;
  onUpload: (file: File) => void;
}

export const UploadCephDialog: React.FunctionComponent<UploadCephDialogProps> = ({
  open,
  onClose,
  onUpload,
  loading,
  ...other
}) => {
  const title = 'Upload Ceph';

  const handleDrop = useCallback(
    (acceptedFiles: File[]) => {
      console.log('files', acceptedFiles);

      if (acceptedFiles.length) {
        onUpload(acceptedFiles[0]);
      }
    },
    [onUpload]
  );

  return (
    <Dialog fullWidth maxWidth="sm" open={open} onClose={onClose} {...other}>
      <DialogTitle sx={{ p: (theme) => theme.spacing(3, 3, 2, 3) }}> {title} </DialogTitle>

      <DialogContent dividers sx={{ pt: 1, pb: 0, border: 'none' }}>
        {loading && <LinearProgress color="primary" />}
        <Upload files={[]} onDrop={handleDrop} disabled={loading} />
      </DialogContent>

      <DialogActions />
    </Dialog>
  );
};
