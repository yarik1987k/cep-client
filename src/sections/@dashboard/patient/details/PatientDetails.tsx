import React, { useState } from 'react';
import { Box, Card, Tab, Tabs } from '@mui/material';
import {Patient, ShortPatient} from '../../../../service/types';
import { Profile, ProfileCover } from '../../__old/user/profile';
import { _userAbout, _userFeeds } from '../../../../_mock/arrays';
import { patientFullName } from '../../../../utils/patient';
import Iconify from '../../../../components/iconify';
import { PatientCephList } from './PatientCephList';

interface PatientDetailsProps {
  patient: Patient;
}

export const PatientDetails: React.FunctionComponent<PatientDetailsProps> = ({ patient }) => {
  const [currentTab, setCurrentTab] = useState('ceph');

  const fullName = patientFullName(patient);

  const TABS = [
    {
      value: 'ceph',
      label: 'Ceph',
      icon: <Iconify icon="ic:round-perm-media" />,
      component: <PatientCephList patient={patient} />,
    },
  ];

  return (
    <>
      <Card
        sx={{
          mb: 3,
          height: 280,
          position: 'relative',
        }}
      >
        {/* eslint-disable-next-line jsx-a11y/aria-role */}
        <ProfileCover name={fullName} role="Patient" cover={_userAbout.cover} />

        <Tabs
          value={currentTab}
          onChange={(event, newValue) => setCurrentTab(newValue)}
          sx={{
            width: 1,
            bottom: 0,
            zIndex: 9,
            position: 'absolute',
            bgcolor: 'background.paper',
            '& .MuiTabs-flexContainer': {
              pr: { md: 3 },
              justifyContent: {
                sm: 'center',
                md: 'flex-end',
              },
            },
          }}
        >
          {TABS.map((tab) => (
            <Tab key={tab.value} value={tab.value} icon={tab.icon} label={tab.label} />
          ))}
        </Tabs>
      </Card>
      {TABS.map((tab) => tab.value === currentTab && <Box key={tab.value}> {tab.component} </Box>)}
    </>
  );
};
