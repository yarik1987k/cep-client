import React, { useState } from 'react';
import { Box, Button, Grid, Table, TableBody, TableContainer } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import Iconify from '../../../../components/iconify';
import { UploadCephDialog } from './UploadCephDialog';
import { useCreateCephMutation } from '../../../../service/ceph';
import { Patient, ShortPatient } from '../../../../service/types';
import { emptyRows, TableEmptyRows, TableHeadCustom, useTable } from '../../../../components/table';
import { CephsTableRow } from './CephsTableRow';
import { PATH_DASHBOARD } from '../../../../routes/paths';

const TABLE_HEAD = [
  { id: 'name', label: 'Name', align: 'left' },
  { id: 'createdAt', label: 'Created' },
  { id: '' },
];

interface PatientCephListProps {
  patient: Patient;
}

export const PatientCephList: React.FunctionComponent<PatientCephListProps> = ({ patient }) => {
  const [openUploadFile, setOpenUploadFile] = useState(false);

  const table = useTable({ defaultRowsPerPage: 10 });
  const navigate = useNavigate();

  const mutation = useCreateCephMutation({
    onSuccess: (ceph) => {
      setOpenUploadFile(false);
      navigate(PATH_DASHBOARD.ceph.ceph(ceph.id));
    },
  });

  const handleOpenUploadFile = () => {
    setOpenUploadFile(true);
  };

  const handleCloseUploadFile = () => {
    if (!mutation.isLoading) {
      setOpenUploadFile(false);
    }
  };

  const {
    dense,
    page,
    order,
    orderBy,
    rowsPerPage,
    //
    selected,
    onSelectRow,
    onSelectAllRows,
    //
    onSort,
    onChangeDense,
    onChangePage,
    onChangeRowsPerPage,
  } = table;
  const denseHeight = dense ? 52 : 72;

  const cephs = patient.cephs || [];

  return (
    <>
      <Grid container spacing={3}>
        <Grid item xs={0} md={8} lg={9} />
        <Grid item xs={12} md={4} lg={3}>
          <Button
            variant="contained"
            fullWidth
            startIcon={<Iconify icon="eva:cloud-upload-fill" />}
            onClick={handleOpenUploadFile}
          >
            Upload Ceph
          </Button>
        </Grid>

        <Grid item xs={12}>
          <Box
            sx={{ px: 1, position: 'relative', borderRadius: 1.5, bgcolor: 'background.neutral' }}
          >
            <TableContainer>
              <Table
                size={dense ? 'small' : 'medium'}
                sx={{
                  minWidth: 960,
                  borderCollapse: 'separate',
                  borderSpacing: '0 8px',
                  '& .MuiTableCell-head': {
                    boxShadow: 'none !important',
                  },
                }}
              >
                <TableHeadCustom
                  order={order}
                  orderBy={orderBy}
                  headLabel={TABLE_HEAD}
                  rowCount={cephs.length}
                  numSelected={selected.length}
                  onSort={onSort}
                  onSelectAllRows={(checked) =>
                    onSelectAllRows(
                      checked,
                      cephs.map((row) => row.id)
                    )
                  }
                  sx={{
                    '& .MuiTableCell-head': {
                      bgcolor: 'transparent',
                    },
                  }}
                />

                <TableBody>
                  {cephs.map((row) => (
                    <CephsTableRow ceph={row} key={row.id} />
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Box>
        </Grid>
      </Grid>

      <UploadCephDialog
        open={openUploadFile}
        onClose={handleCloseUploadFile}
        loading={mutation.isLoading}
        onUpload={(file) => {
          mutation.mutate({ file, patientId: patient.id });
        }}
      />
    </>
  );
};
