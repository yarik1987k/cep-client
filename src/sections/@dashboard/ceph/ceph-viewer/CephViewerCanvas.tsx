// eslint-disable-next-line max-classes-per-file
import React, { useEffect, useMemo, useRef } from 'react';
import {
  CephViewerRenderContext,
  CephViewerRenderPoint,
  OnChangeCephCoordinateFn,
  PatchedCoordinate,
} from './types';
import { Dimensions } from '../../../../service/types';
import { useCephViewerState } from './useCephViewerState';
import { getCanvasOffset } from './utils';

interface CephViewerCanvasProps {
  canvasSize: Dimensions;
  image: HTMLImageElement;
  coordinates: PatchedCoordinate[];
  onChange: OnChangeCephCoordinateFn;
}

export const CephViewerCanvas: React.FunctionComponent<CephViewerCanvasProps> = ({
  canvasSize,
  image,
  coordinates,
  onChange,
}) => {
  // --------------
  const canvasRef = useRef<HTMLCanvasElement>(null);

  const renderContext = useMemo<CephViewerRenderContext>(
    () => ({ coordinates, canvasRef, canvasSize, actions: { onChange } }),
    [coordinates, canvasSize, onChange]
  );
  const renderState = useCephViewerState(renderContext);

  useEffect(() => {
    const canvas = canvasRef.current?.getContext('2d');
    if (!canvas) return;

    canvas.clearRect(0, 0, canvasSize.width, canvasSize.height);
    canvas.drawImage(image, 0, 0, canvasSize.width, canvasSize.height);

    renderState.points.forEach((point) => {
      renderPoint(canvas, point);
    });
  }, [canvasSize, image, renderState]);

  const canvasOffset = getCanvasOffset(canvasRef);

  return (
    <>
      <canvas
        width={canvasSize.width}
        height={canvasSize.height}
        ref={canvasRef}
        onMouseDown={renderState.canvas?.onMouseDown}
        onMouseMove={renderState.canvas?.onMouseMove}
        onMouseUp={renderState.canvas?.onMouseUp}
        style={{
          cursor: renderState.cursor || 'inherit',
        }}
      />
      {renderState.hover && (
        <div
          style={{
            position: 'absolute',
            left: canvasOffset.left + renderState.hover.x + 2,
            top: canvasOffset.top + renderState.hover.y - 32 - 2,
            backgroundColor: 'rgba(0, 0, 0, 0.7)',
            color: '#fff',
            padding: '4px 8px',
            borderRadius: '4px',
          }}
        >
          {renderState.hover.text}
        </div>
      )}
    </>
  );
};

const renderPoint = (canvas: CanvasRenderingContext2D, point: CephViewerRenderPoint) => {
  canvas.beginPath();
  canvas.arc(point.x, point.y, 3, 0, 2 * Math.PI);
  canvas.fillStyle = point.color;
  canvas.fill();
};
