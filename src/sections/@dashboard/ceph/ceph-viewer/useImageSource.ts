import { useEffect, useState } from 'react';
import { Maybe } from 'yup';

export const useImageSource = (link: string): Maybe<HTMLImageElement> => {
  const [image, setImage] = useState<Maybe<HTMLImageElement>>(null);

  useEffect(() => {
    const imageSource = new Image();
    imageSource.onload = function imageSourceReceiver() {
      // @ts-ignore
      setImage(this);
    };
    imageSource.src = link;
  }, [link]);

  return image;
};
