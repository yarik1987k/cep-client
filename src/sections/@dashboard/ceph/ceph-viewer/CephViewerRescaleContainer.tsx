import React, { useMemo } from 'react';
import { Maybe } from 'yup';
import { CephCoordinate, Dimensions, Point } from '../../../../service/types';
import { OnChangeCephCoordinateFn } from './types';

export interface CephViewerRescaleContainerProps {
  imageSize: Dimensions;
  containerSize: Dimensions;
  coordinates: CephCoordinate[];
  onChange: OnChangeCephCoordinateFn;
  children: (
    coordinates: CephCoordinate[],
    canvasSize: Dimensions,
    onChange: OnChangeCephCoordinateFn
  ) => JSX.Element;
}

export const CephViewerRescaleContainer: React.FunctionComponent<
  CephViewerRescaleContainerProps
> = ({ imageSize, containerSize, coordinates, onChange, children }) => {
  const canvasSizeAndScale = useMemo(
    () => measureCanvasSize(containerSize, imageSize),
    [containerSize, imageSize]
  );

  const scaledCoordinates = useMemo(
    () => canvasSizeAndScale && scaleCoordinate(coordinates, canvasSizeAndScale.scale),
    [coordinates, canvasSizeAndScale]
  );

  const rescaledOnChange = (change: { index: number; x: number; y: number }) => {
    if (!canvasSizeAndScale || !canvasSizeAndScale) {
      return;
    }
    const rescaled = rescaleCoordinate(change, canvasSizeAndScale.scale);
    onChange({ index: change.index, x: rescaled.x, y: rescaled.y });
  };

  if (!scaledCoordinates || !canvasSizeAndScale) {
    return null;
  }

  return children(scaledCoordinates, canvasSizeAndScale.size, rescaledOnChange);
};

const measureCanvasSize = (
  canvasSize: Dimensions,
  imageSize: Dimensions
): Maybe<{ size: Dimensions; scale: number }> => {
  if (!canvasSize.width || !imageSize.width) {
    return null;
  }
  const scale = imageSize.width / canvasSize.width;

  return {
    size: { width: imageSize.width / scale, height: imageSize.height / scale },
    scale,
  };
};

const scaleCoordinate = (coordinates: CephCoordinate[], scale: number) =>
  coordinates.map((coordinate) => ({
    ...coordinate,
    x: coordinate.x / scale,
    y: coordinate.y / scale,
  }));

const rescaleCoordinate = (coordinate: Point, scale: number): Point => ({
  x: coordinate.x * scale,
  y: coordinate.y * scale,
});
