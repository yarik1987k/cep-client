import { CephCoordinate, Dimensions } from '../../../../service/types';
import { MouseEventHandler, RefObject } from 'react';

export type OnChangeCephCoordinateFn = (change: { index: number; x: number; y: number }) => void;

export type PatchedCoordinate = CephCoordinate & { patch?: boolean };

export type CoordinatesPatch = { [key in number]: CephCoordinate };

export interface CephViewerStateIdle {
  state: 'idle';
}

export interface CephViewerStateHover {
  state: 'hover';
  payload: {
    index: number;
  };
}

export interface CephViewerStateDrag {
  state: 'drag';
  payload: {
    index: number;
    x: number;
    y: number;
  };
}

export type CephViewerState = CephViewerStateIdle | CephViewerStateHover | CephViewerStateDrag;

export interface CephViewerRenderPoint {
  x: number;
  y: number;
  color: string;
}

export interface CephViewerRenderState {
  points: CephViewerRenderPoint[];
  hover?: {
    x: number;
    y: number;
    text: string;
  };
  cursor?: 'pointer';
  canvas?: {
    onMouseDown?: MouseEventHandler;
    onMouseMove?: MouseEventHandler;
    onMouseUp?: MouseEventHandler;
  };
}

export interface CephViewerRenderContext {
  canvasRef: RefObject<HTMLCanvasElement>;
  coordinates: PatchedCoordinate[];
  canvasSize: Dimensions;
  actions: {
    onChange: OnChangeCephCoordinateFn;
  };
}

export interface CephViewerController {
  state: {
    idle: () => void;
    hover: (index: number) => void;
    drag: (index: number, x: number, y: number) => void;
  };
}

export interface CanvasMouseEffectEvent {
  mouseCanvasPosition: {
    x: number;
    y: number;
  };
  mouseDelta: {
    dx: number;
    dy: number;
  };
}
