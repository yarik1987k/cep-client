import { MouseEvent, RefObject } from 'react';
import { Maybe } from 'yup';
import { CanvasMouseEffectEvent } from './types';
import { Point } from '../../../../service/types';

export const wrapMouseEvent = (
  event: MouseEvent,
  canvasRef: RefObject<HTMLCanvasElement>
): CanvasMouseEffectEvent => {
  const canvasOffset = getCanvasOffset(canvasRef);
  const canvasPosition = {
    x: event.pageX - canvasOffset.left,
    y: event.pageY - canvasOffset.top,
  };
  const mouseDelta = {
    dx: event.movementX,
    dy: event.movementY,
  };
  return { mouseCanvasPosition: canvasPosition, mouseDelta };
};

export const getCanvasOffset = (canvasRef: RefObject<HTMLCanvasElement>) => ({
  left: canvasRef?.current?.offsetLeft || 0,
  top: canvasRef?.current?.offsetTop || 0,
});

export const findNearedInRange = (
  coordinates: Point[],
  position: Point,
  range: number = 10
): Maybe<{ index: number }> => {
  let bestRange = -1;
  let bestPoint: Maybe<{ index: number }> = null;
  const radiusSquare = range ** 2;
  coordinates.forEach((coordinate, index) => {
    const distanceSquare = (position.x - coordinate.x) ** 2 + (position.y - coordinate.y) ** 2;
    if (distanceSquare > radiusSquare) {
      return;
    }

    if (bestRange === -1 || bestRange > distanceSquare) {
      bestRange = distanceSquare;
      bestPoint = { index };
    }
  });
  return bestPoint;
};
