import { useState } from 'react';
import {
  CanvasMouseEffectEvent,
  CephViewerController,
  CephViewerRenderContext,
  CephViewerRenderPoint,
  CephViewerRenderState,
  CephViewerState,
  CephViewerStateDrag,
  CephViewerStateHover,
  CephViewerStateIdle,
  PatchedCoordinate,
} from './types';
import { cephPointNames } from './point-names';
import { findNearedInRange, wrapMouseEvent } from './utils';

export const useCephViewerState = (context: CephViewerRenderContext): CephViewerRenderState => {
  const [viewerState, setViewerState] = useState<CephViewerState>(() => ({ state: 'idle' }));

  const controller: CephViewerController = {
    state: {
      idle: () => {
        setViewerState({ state: 'idle' });
      },
      hover: (index: number) => {
        setViewerState({ state: 'hover', payload: { index } });
      },
      drag: (index: number, x: number, y: number) => {
        setViewerState({ state: 'drag', payload: { index, x, y } });
      },
    },
  };

  // eslint-disable-next-line default-case
  switch (viewerState.state) {
    case 'idle':
      return getIdleRenderState(context, viewerState, controller);
    case 'hover':
      return getHoverRenderState(context, viewerState, controller);
    case 'drag':
      return getDragRenderState(context, viewerState, controller);
  }

  throw Error(`Ceph viewer state was not handle.`);
};

const CEPH_VIEWER_POINT_COLORS = {
  default: 'red',
  edit: 'yellow',
  hover: 'blue',
  drag: 'green',
};

const defaultPoints = (coordinates: PatchedCoordinate[]) =>
  coordinates.map((coordinate) => ({
    x: coordinate.x,
    y: coordinate.y,
    color: coordinate.patch ? CEPH_VIEWER_POINT_COLORS.edit : CEPH_VIEWER_POINT_COLORS.default,
  }));

const getIdleRenderState = (
  context: CephViewerRenderContext,
  state: CephViewerStateIdle,
  controller: CephViewerController
): CephViewerRenderState => {
  const handleMouseMove = (event: CanvasMouseEffectEvent) => {
    const nearest = findNearedInRange(context.coordinates, event.mouseCanvasPosition);
    if (nearest) {
      controller.state.hover(nearest.index);
    }
  };

  return {
    points: defaultPoints(context.coordinates),
    canvas: {
      onMouseMove: (event) => handleMouseMove(wrapMouseEvent(event, context.canvasRef)),
    },
  };
};

const patchPointFn =
  (patchIndex: number, patch: Partial<CephViewerRenderPoint>) =>
  (point: CephViewerRenderPoint, index: number) =>
    patchIndex !== index ? point : { ...point, ...patch };

const getHoverRenderState = (
  context: CephViewerRenderContext,
  state: CephViewerStateHover,
  controller: CephViewerController
): CephViewerRenderState => {
  const point = context.coordinates[state.payload.index];
  const hover = {
    x: point.x,
    y: point.y,
    text: cephPointNames[state.payload.index],
  };

  const handleMouseMove = (event: CanvasMouseEffectEvent) => {
    const nearest = findNearedInRange(context.coordinates, event.mouseCanvasPosition);
    if (!nearest) {
      controller.state.idle();
      return;
    }
    if (nearest.index !== state.payload.index) {
      controller.state.hover(nearest.index);
    }
  };

  const handleMouseDown = () => {
    const coordinate = context.coordinates[state.payload.index];
    controller.state.drag(state.payload.index, coordinate.x, coordinate.y);
  };

  return {
    points: defaultPoints(context.coordinates).map(
      patchPointFn(state.payload.index, { color: CEPH_VIEWER_POINT_COLORS.hover })
    ),
    cursor: 'pointer',
    hover,
    canvas: {
      onMouseMove: (event) => handleMouseMove(wrapMouseEvent(event, context.canvasRef)),
      onMouseDown: handleMouseDown,
    },
  };
};

const getDragRenderState = (
  context: CephViewerRenderContext,
  state: CephViewerStateDrag,
  controller: CephViewerController
): CephViewerRenderState => {
  const { index, x, y } = state.payload;

  const handleMouseMove = (event: CanvasMouseEffectEvent) => {
    const newX = Math.min(
      Math.max(0, state.payload.x + event.mouseDelta.dx),
      context.canvasSize.width
    );
    const newY = Math.min(
      Math.max(0, state.payload.y + event.mouseDelta.dy),
      context.canvasSize.height
    );
    controller.state.drag(state.payload.index, newX, newY);
  };
  const handleMouseUp = () => {
    context.actions.onChange(state.payload);
    controller.state.idle();
  };

  return {
    points: defaultPoints(context.coordinates).map(
      patchPointFn(index, {
        color: CEPH_VIEWER_POINT_COLORS.drag,
        x,
        y,
      })
    ),
    canvas: {
      onMouseMove: (event) => handleMouseMove(wrapMouseEvent(event, context.canvasRef)),
      onMouseUp: handleMouseUp,
    },
  };
};
