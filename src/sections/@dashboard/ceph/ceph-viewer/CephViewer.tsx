import React from 'react';
import { Box } from '@mui/material';
import { useImageSource } from './useImageSource';
import { CephViewerCanvas } from './CephViewerCanvas';
import { CephCoordinate, Dimensions } from '../../../../service/types';
import { useThrottledElementSize } from '../../../../hooks/useThrottledElementSize';
import { CephViewerRescaleContainer } from './CephViewerRescaleContainer';
import { OnChangeCephCoordinateFn } from './types';

interface CephViewerProps {
  url: string;
  imageSize: Dimensions;
  coordinates: CephCoordinate[];
  onChange: OnChangeCephCoordinateFn;
}

export const CephViewer: React.FunctionComponent<CephViewerProps> = ({
  url,
  imageSize,
  coordinates,
  onChange,
}) => {
  const [ref, containerSize] = useThrottledElementSize();

  const image = useImageSource(url);

  if (!image) {
    return null;
  }

  return (
    <Box ref={ref} style={{ minWidth: 100 }}>
      <CephViewerRescaleContainer
        imageSize={imageSize}
        containerSize={containerSize}
        coordinates={coordinates}
        onChange={onChange}
      >
        {(scaledCoordinates, canvasSize, rescaledOnChange) => (
          <CephViewerCanvas
            canvasSize={canvasSize}
            image={image}
            coordinates={scaledCoordinates}
            onChange={rescaledOnChange}
          />
        )}
      </CephViewerRescaleContainer>
    </Box>
  );
};
