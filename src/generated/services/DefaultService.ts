/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { ArticlesResponse } from '../models/ArticlesResponse';
import type { City } from '../models/City';
import type { SignedPutResponse } from '../models/SignedPutResponse';
import type { User } from '../models/User';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class DefaultService {

    /**
     * @returns string OK
     * @throws ApiError
     */
    public static get(): CancelablePromise<string> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/',
        });
    }

    /**
     * @param sort
     * @returns ArticlesResponse OK
     * @throws ApiError
     */
    public static getArticles(
        sort?: string,
    ): CancelablePromise<Array<ArticlesResponse>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/articles',
            query: {
                'sort': sort,
            },
        });
    }

    /**
     * @returns string OK
     * @throws ApiError
     */
    public static getJsonKotlinxSerialization(): CancelablePromise<string> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/json/kotlinx-serialization',
        });
    }

    /**
     * @param connection Websocket Connection parameter
     * @param upgrade Websocket Upgrade parameter
     * @param secWebSocketKey Websocket Sec-WebSocket-Key parameter
     * @returns void
     * @throws ApiError
     */
    public static getWs(
        connection: string,
        upgrade: string,
        secWebSocketKey: string,
    ): CancelablePromise<void> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/ws',
            headers: {
                'Connection': connection,
                'Upgrade': upgrade,
                'Sec-WebSocket-Key': secWebSocketKey,
            },
        });
    }

    /**
     * Create city
     * @param requestBody
     * @returns number Created
     * @throws ApiError
     */
    public static postCities(
        requestBody: City,
    ): CancelablePromise<number> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/cities',
            body: requestBody,
            mediaType: '*/*',
        });
    }

    /**
     * Delete city
     * @param id
     * @returns any OK
     * @throws ApiError
     */
    public static deleteCities(
        id: number,
    ): CancelablePromise<Record<string, any>> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/cities/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * Read city
     * @param id
     * @returns City OK
     * @throws ApiError
     */
    public static getCities(
        id: number,
    ): CancelablePromise<City> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/cities/{id}',
            path: {
                'id': id,
            },
            errors: {
                404: `Not Found`,
            },
        });
    }

    /**
     * Update city
     * @param id
     * @param requestBody
     * @returns any OK
     * @throws ApiError
     */
    public static putCities(
        id: number,
        requestBody: City,
    ): CancelablePromise<Record<string, any>> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/cities/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: '*/*',
        });
    }

    /**
     * Create user
     * @param requestBody
     * @returns number Created
     * @throws ApiError
     */
    public static postUsers(
        requestBody: User,
    ): CancelablePromise<number> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/users',
            body: requestBody,
            mediaType: '*/*',
        });
    }

    /**
     * Delete user
     * @param id
     * @returns any OK
     * @throws ApiError
     */
    public static deleteUsers(
        id: number,
    ): CancelablePromise<Record<string, any>> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/users/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * Read user
     * @param id
     * @returns User OK
     * @throws ApiError
     */
    public static getUsers(
        id: number,
    ): CancelablePromise<User> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/users/{id}',
            path: {
                'id': id,
            },
            errors: {
                404: `Not Found`,
            },
        });
    }

    /**
     * Update user
     * @param id
     * @param requestBody
     * @returns any OK
     * @throws ApiError
     */
    public static putUsers(
        id: number,
        requestBody: User,
    ): CancelablePromise<Record<string, any>> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/users/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: '*/*',
        });
    }

    /**
     * @returns string OK
     * @throws ApiError
     */
    public static getSessionIncrement(): CancelablePromise<string> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/session/increment',
        });
    }

    /**
     * @returns SignedPutResponse OK
     * @throws ApiError
     */
    public static postApiFileSignedPut(): CancelablePromise<SignedPutResponse> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/file/signed-put',
        });
    }

}
