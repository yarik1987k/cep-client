/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type SignedPutResponse = {
    url?: string;
    headers?: string;
};

