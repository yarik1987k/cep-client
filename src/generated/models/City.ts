/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type City = {
    name?: string;
    population?: number;
};

