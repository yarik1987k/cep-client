import { useEffect, useRef, useState } from 'react';

export const useThrottle = <T>(value: T, delay = 100): T => {
  const [throttled, setThrottled] = useState(value);

  const timerRef = useRef<number | null>(null);
  const lastValueRef = useRef<T | null>(null);

  useEffect(() => {
    if (!timerRef.current) {
      setThrottled(value);
      const id = setTimeout(() => {
        timerRef.current = null;
        if (lastValueRef.current) {
          setThrottled(lastValueRef.current);
          lastValueRef.current = null;
        }
      }, delay);

      // @ts-ignore
      timerRef.current = id;
    } else {
      lastValueRef.current = value;
    }
  }, [value, delay]);

  return throttled;
};
