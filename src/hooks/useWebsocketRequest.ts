import { useContext } from 'react';
import { WebsocketContext } from '../websocket';
import { WebsocketListener } from '../types';

export const useWebsocket = () => {
  const context = useContext(WebsocketContext);

  if (!context) throw new Error('useAuthContext context must be use inside AuthProvider');

  return context;
};

export const useWebsocketRequest = (type: string) => {
  const ws = useWebsocket();

  const request = <P>(payload: P): Promise<any> => {
    const rid = `${type}:${Date.now()}`;

    return new Promise((resolve, reject) => {
      let resolved = false;

      const messageWithRid = {
        type,
        rid,
        payload,
      };

      const listener: WebsocketListener = (data) => {
        ws.removeRidListener(rid, listener);
        resolved = true;
        resolve(data.payload);
      };

      ws.addRidListener(rid, listener);
      ws.sendMessage(messageWithRid);

      setTimeout(() => {
        if (!resolved) {
          resolved = true;
          ws.removeRidListener(rid, listener);
          reject(Error('Request canceled by timeout'));
        }
      }, 10000);
    });
  };

  return request;
};
