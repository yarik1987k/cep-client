import { useElementSize } from 'usehooks-ts';
import { useThrottle } from './useThrottle';

export const useThrottledElementSize = <T extends HTMLElement = HTMLDivElement>(): ReturnType<
  typeof useElementSize<T>
> => {
  const [ref, size] = useElementSize<T>();

  const throttledSize = useThrottle(size);

  return [ref, throttledSize];
};
