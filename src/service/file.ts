import { useMutation, useQuery, UseQueryOptions } from 'react-query';
import { DefaultService } from '../generated';
import { useWebsocketRequest } from '../hooks/useWebsocketRequest';
import { CreateGetFileUrlPayload, GetPatientResponsePayload } from './types';

const uploadFile = async (file: File, url: string) => {
  const response = await fetch(url, {
    method: 'PUT',
    body: file,
  });
  return response.text();
};

export const useUploadFileMutation = () => {
  const mutation = useMutation({
    mutationFn: async (file: File) => {
      const signedPut = await DefaultService.postApiFileSignedPut();
      console.log('signedPut', signedPut);
      console.log('signedPut', signedPut);
      await uploadFile(file, signedPut.url ?? '');
    },
  });

  return mutation;
};

export const useGetFileQuery = (
  fileId: string,
  options: UseQueryOptions<unknown, unknown, CreateGetFileUrlPayload> = {}
) => {
  const request = useWebsocketRequest('create-get-file-url-request');
  return useQuery({
    queryKey: ['file', fileId],
    queryFn: (): Promise<CreateGetFileUrlPayload> => request(fileId),
    ...options,
  });
};
