import { useMutation, UseMutationOptions, useQuery, UseQueryOptions } from 'react-query';
import { useWebsocketRequest } from '../hooks/useWebsocketRequest';
import { Ceph, GetPatientResponsePayload, StorageFile } from './types';

export interface CreateCephMutationVariables {
  patientId: string;
  file: File;
}

export const useGetCephQuery = (
  id: string,
  options: UseQueryOptions<unknown, unknown, Ceph> = {}
) => {
  const request = useWebsocketRequest('get-ceph-request');

  return useQuery({
    queryKey: ['ceph', id],
    queryFn: (): Promise<GetPatientResponsePayload> => request({ id }).then((it) => it.ceph),
    ...options,
  });
};

export const useUpdateCeph = (options: UseMutationOptions<Ceph, unknown, Ceph>) => {
  const request = useWebsocketRequest('update-ceph-request');

  return useMutation({
    mutationFn: (ceph: Ceph): Promise<Ceph> => request({ ceph }).then((it) => it.ceph),
    ...options,
  });
};

export const useCreateCephMutation = (
  options: UseMutationOptions<Ceph, unknown, CreateCephMutationVariables> = {}
) => {
  const createPresignedPutRequest = useWebsocketRequest('create-upload-file-url-request');
  const confirmUploadRequest = useWebsocketRequest('confirm-upload-file-request');
  const createCephRequest = useWebsocketRequest('create-ceph-request');

  return useMutation({
    mutationFn: async ({ patientId, file }: CreateCephMutationVariables) => {
      const signedPut: { url: string; fileId: string } = await createPresignedPutRequest(null);

      await uploadFile(file, signedPut.url ?? '');

      const response: { file: StorageFile } = await confirmUploadRequest({
        fileId: signedPut.fileId,
      });

      const ceph: Ceph = await createCephRequest({ patientId, fileId: response.file.id });
      return ceph;
    },
    ...options,
  });
};

const uploadFile = async (file: File, url: string) => {
  const response = await fetch(url, {
    method: 'PUT',
    body: file,
  });
  return response.text();
};
