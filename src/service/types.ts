import { Maybe } from 'yup';
import { UUID } from '../types';

export interface ShortPatient {
  id: UUID;
  firstName: Maybe<string>;
  lastName: Maybe<string>;
  description: Maybe<string>;
}

export interface Patient extends ShortPatient {
  cephs: Ceph[];
}

export interface StorageFile {
  id: UUID;
  uploadedBy: UUID;
  uploaded: boolean;
}

export interface Ceph {
  id: UUID;
  cephFile: UUID;
  createdBy: UUID;
  createdAt: string;
  patient: UUID;
  result: CephProcessingResult | null;
}

export interface CephProcessingResult {
  traced: boolean;
  exifOrientation: number;
  imgQuality: number;
  scale: {
    confidence: number;
    value: number;
    points: Point[];
  };
  coordinates: CephCoordinate[];
  imageSize: Dimensions;
}

export interface Dimensions {
  width: number;
  height: number;
}

export interface ScaledDimensions extends Dimensions {
  scale: number;
}

export interface Point {
  x: number;
  y: number;
}

export interface CephCoordinate {
  x: number;
  y: number;
  c: number;
}

export interface CreatePatientResponsePayload {
  id: string;
}

export type GetPatientResponsePayload = Patient;

export type GetPatientsResponsePayload = Patient[];

export type CreateGetFileUrlPayload = { fileId: UUID; url: string };
