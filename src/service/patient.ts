import { useMutation, UseMutationOptions, useQuery, UseQueryOptions } from 'react-query';
import { useWebsocketRequest } from '../hooks/useWebsocketRequest';
import {
  CreatePatientResponsePayload,
  GetPatientResponsePayload,
  GetPatientsResponsePayload,
  ShortPatient,
} from './types';

export const useGetPatientQuery = (
  id: string,
  options: UseQueryOptions<unknown, unknown, GetPatientResponsePayload> = {}
) => {
  const request = useWebsocketRequest('get-patient-request');

  return useQuery({
    queryKey: ['get-patient-request', id],
    queryFn: (): Promise<GetPatientResponsePayload> => request({ id }),
    ...options,
  });
};

export const useGetPatientsQuery = (
  options: UseQueryOptions<unknown, unknown, GetPatientsResponsePayload> = {}
) => {
  const request = useWebsocketRequest('get-patients-request');

  return useQuery({
    queryKey: 'get-patients-request',
    queryFn: (): Promise<GetPatientsResponsePayload> => request(null),
    ...options,
  });
};

export const useCreatePatientMutation = (
  options: UseMutationOptions<CreatePatientResponsePayload, unknown, ShortPatient> = {}
) => {
  const request = useWebsocketRequest('create-patient-request');

  return useMutation({
    mutationFn: async (patient: ShortPatient): Promise<CreatePatientResponsePayload> => {
      // TODO remove (just to check loading status)
      await new Promise((resolve) => setTimeout(resolve, 3000));
      return request(patient);
    },
    ...options,
  });
};
