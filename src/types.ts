export type UUID = string;

export type WebsocketMessage<P = unknown> = {
  type: string;
  rid?: string;
  payload: P;
};

export type WebsocketListener<P = unknown> = (message: WebsocketMessage<P>) => void;

