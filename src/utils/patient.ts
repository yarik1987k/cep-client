import { ShortPatient } from '../service/types';

export const shortUUID = (id: string) => id.split('-')[0];

export const patientFullName = (patient: ShortPatient) =>
  [patient.firstName, patient.lastName].filter((it) => it).join(' ');
