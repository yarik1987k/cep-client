import axios from 'axios';
// config
import { HOST_API_KEY } from '../config-global';

// ----------------------------------------------------------------------

export const cancelTokenSource = axios.CancelToken.source();

const axiosInstance = axios.create({ baseURL: HOST_API_KEY, cancelToken: cancelTokenSource.token });

axiosInstance.interceptors.response.use(
  (response) => response,
  (error) => Promise.reject((error.response && error.response.data) || 'Something went wrong')
);

export default axiosInstance;
