import { createContext, useEffect, useReducer, useCallback, useMemo } from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { CognitoUserPool, CognitoUserSession } from 'amazon-cognito-identity-js';
// eslint-disable-next-line import/no-extraneous-dependencies
import { CognitoHostedUIIdentityProvider } from '@aws-amplify/auth';
// utils
import { Amplify, Auth, Hub } from 'aws-amplify';
import { useNavigate } from 'react-router-dom';
import { Maybe } from 'yup';
// routes
import { PATH_AUTH } from '../routes/paths';
// config
import { COGNITO_API } from '../config-global';
//
import {
  ActionMapType,
  ApplicationCognitoUser,
  AuthenticatedUser,
  AuthStateType,
  AWSCognitoContextType,
} from './types';
import axiosInstance from '../utils/axios';

// ----------------------------------------------------------------------

// NOTE:
// We only build demo at basic level.
// Customer will need to do some extra handling yourself if you want to extend the logic and other features...

// ----------------------------------------------------------------------

// --------- Amplify
const amplifyConfig = {
  aws_project_region: COGNITO_API.region || '',
  aws_cognito_region: COGNITO_API.region || '',
  aws_user_pools_id: COGNITO_API.userPoolId || '',
  aws_user_pools_web_client_id: COGNITO_API.clientId || '',
  oauth: {
    domain: COGNITO_API.cognitoDomain,
    scope: ['email', 'profile', 'openid', 'aws.cognito.signin.user.admin'],
    redirectSignIn: `${COGNITO_API.applicationUrl}/dashboard`,
    redirectSignOut: COGNITO_API.applicationUrl,
    responseType: 'code',
  },
  federationTarget: 'COGNITO_USER_POOLS',
};

Amplify.configure(amplifyConfig);

function listenToAutoSignInEvent(
  onSuccess: (user: ApplicationCognitoUser) => void,
  onFailure: () => void
): () => void {
  return Hub.listen('auth', ({ payload }) => {
    const { event } = payload;
    if (event === 'autoSignIn') {
      const user: ApplicationCognitoUser = payload.data;
      onSuccess(user);
    } else if (event === 'autoSignIn_failure') {
      onFailure();
    }
  });
}

enum Types {
  GUEST = 'GUEST',
  AUTH = 'AUTH',
  LOGOUT = 'LOGOUT',
}

type Payload = {
  [Types.GUEST]: undefined;
  [Types.AUTH]: {
    user: AuthenticatedUser;
    session: CognitoUserSession;
  };
  [Types.LOGOUT]: undefined;
};

type ActionsType = ActionMapType<Payload>[keyof ActionMapType<Payload>];

// ----------------------------------------------------------------------

const initialState: AuthStateType = {
  isAuthenticated: false,
  isInitialized: false,
  user: null,
};

const reducer = (state: AuthStateType, action: ActionsType) => {
  if (action.type === Types.AUTH) {
    // const token = action.payload.session.getIdToken().getJwtToken();
    // axios.defaults.headers.common.Authorization = `Bearer ${token}`;

    return {
      isInitialized: true,
      isAuthenticated: true,
      user: action.payload.user,
    };
  }
  if (action.type === Types.GUEST) {
    return {
      isInitialized: true,
      isAuthenticated: false,
      user: null,
    };
  }
  if (action.type === Types.LOGOUT) {
    return {
      isInitialized: true,
      isAuthenticated: false,
      user: null,
    };
  }
  return state;
};

// ----------------------------------------------------------------------

export const AuthContext = createContext<AWSCognitoContextType | null>(null);

// ----------------------------------------------------------------------

const userPool = new CognitoUserPool({
  UserPoolId: COGNITO_API.userPoolId || '',
  ClientId: COGNITO_API.clientId || '',
});

type AuthProviderProps = {
  children: React.ReactNode;
};

export function AuthProvider({ children }: AuthProviderProps) {
  const [state, dispatch] = useReducer(reducer, initialState);
  const navigate = useNavigate();

  const initSession = async () => {
    const user = await Auth.currentUserPoolUser();
    return cognitoUserToSession(user);
  };

  const initialize = useCallback(async () => {
    try {
      const session = await initSession();
      const cognitoSession = await Auth.currentSession();
      dispatch({
        type: Types.AUTH,
        payload: {
          user: session,
          session: cognitoSession,
        },
      });
    } catch {
      dispatch({
        type: Types.GUEST,
      });
    }
  }, []);

  useEffect(() => {
    let cancel: Maybe<() => void> = null;
    try {
      cancel = listenToAutoSignInEvent(
        (user) => {
          const session = cognitoUserToSession(user);
          Auth.currentSession()
            .then((cognitoSession) => {
              dispatch({
                type: Types.AUTH,
                payload: {
                  user: session,
                  session: cognitoSession,
                },
              });
            })
            .catch((err) => {
              dispatch({ type: Types.GUEST });
            });
        },
        () => {
          navigate(PATH_AUTH.login);
        }
      );
    } catch (err) {
      console.log('err', err);
    }

    initialize().catch((err) => console.error(err));

    return () => {
      if (cancel) {
        cancel();
      }
    };
  }, [initialize, navigate]);

  // LOGIN
  const login = useCallback(async (email: string, password: string) => {
    const user: ApplicationCognitoUser = await Auth.signIn(email, password);
    const session = await Auth.currentSession();
    dispatch({
      type: Types.AUTH,
      payload: {
        user: cognitoUserToSession(user),
        session,
      },
    });
  }, []);

  // REGISTER
  const register = useCallback(
    async (email: string, password: string, firstName: string, lastName: string) => {
      const response = await Auth.signUp({
        username: email,
        password,
        attributes: {
          given_name: firstName,
          family_name: lastName,
        },
        autoSignIn: {
          enabled: true,
        },
      });

      if (!response.userConfirmed) {
        navigate(`${PATH_AUTH.verify}?username=${email}`);
      } else {
        window.location.href = PATH_AUTH.login;
      }
    },
    [navigate]
  );

  const confirm = useCallback(async (code: string, username: string) => {
    await Auth.confirmSignUp(username, code);
  }, []);

  // LOGOUT
  const logout = useCallback(async () => {
    // TODO logout here
    await logoutRequest();

    const cognitoUser = userPool.getCurrentUser();

    if (cognitoUser) {
      cognitoUser.signOut();
      dispatch({
        type: Types.LOGOUT,
      });
    }
  }, []);

  const memoizedValue: AWSCognitoContextType = useMemo(
    () => ({
      isInitialized: state.isInitialized,
      isAuthenticated: state.isAuthenticated,
      user: state.user,
      method: 'cognito',
      login,
      confirm,
      loginWithGoogle: () => {
        Auth.federatedSignIn({ provider: CognitoHostedUIIdentityProvider.Google }).catch((err) =>
          console.error(err)
        );
      },
      loginWithGithub: () => {},
      loginWithTwitter: () => {},
      register,
      logout,
    }),
    [state.isAuthenticated, state.isInitialized, state.user, login, register, logout, confirm]
  );

  return <AuthContext.Provider value={memoizedValue}>{children}</AuthContext.Provider>;
}

function cognitoUserToSession(user: ApplicationCognitoUser): AuthenticatedUser {
  const attrs = user.attributes;
  const displayName =
    attrs.given_name && attrs.family_name
      ? `${attrs.given_name} ${attrs.family_name}`
      : user.getUsername();
  return {
    username: user.getUsername(),
    confirmed: user.attributes.email_verified,
    email: user.attributes.email,
    firstName: user.attributes.given_name,
    lastName: user.attributes.family_name,
    displayName,
    cognitoUser: user,
  };
}

const logoutRequest = async (): Promise<void> => axiosInstance.post('/api/auth/logout', undefined);
