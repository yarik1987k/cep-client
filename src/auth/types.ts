// ----------------------------------------------------------------------

// eslint-disable-next-line import/no-extraneous-dependencies
import { CognitoUser } from 'amazon-cognito-identity-js';

export interface ApplicationCognitoUserAttributes {
  email: string;
  email_verified: boolean;
  family_name?: string;
  given_name?: string;
  sub: string;
}

export type ApplicationCognitoUser = CognitoUser & { attributes: ApplicationCognitoUserAttributes };


export type ActionMapType<M extends { [index: string]: any }> = {
  [Key in keyof M]: M[Key] extends undefined
    ? {
        type: Key;
      }
    : {
        type: Key;
        payload: M[Key];
      };
};

export interface AuthenticatedUser {
  username: string;
  confirmed: boolean;
  email: string;
  firstName?: string;
  lastName?: string;
  displayName: string;
  photoURL?: string;
  cognitoUser?: ApplicationCognitoUser;
}

export type AuthUserType = null | AuthenticatedUser;
// export type AuthUserType = null | Record<string, any>;

export type AuthStateType = {
  isAuthenticated: boolean;
  isInitialized: boolean;
  user: AuthUserType;
};

// ----------------------------------------------------------------------

export type JWTContextType = {
  method: string;
  isAuthenticated: boolean;
  isInitialized: boolean;
  user: AuthUserType;
  login: (email: string, password: string) => Promise<void>;
  register: (email: string, password: string, firstName: string, lastName: string) => Promise<void>;
  logout: () => void;
  loginWithGoogle?: () => void;
  loginWithGithub?: () => void;
  loginWithTwitter?: () => void;
};

export type FirebaseContextType = {
  method: string;
  isAuthenticated: boolean;
  isInitialized: boolean;
  user: AuthUserType;
  login: (email: string, password: string) => void;
  register: (email: string, password: string, firstName: string, lastName: string) => void;
  logout: () => void;
  loginWithGoogle?: () => void;
  loginWithGithub?: () => void;
  loginWithTwitter?: () => void;
};

export type AWSCognitoContextType = {
  method: string;
  isAuthenticated: boolean;
  isInitialized: boolean;
  user: AuthUserType;
  login: (email: string, password: string) => void;
  register: (email: string, password: string, firstName: string, lastName: string) => void;
  logout: () => Promise<void>;
  loginWithGoogle?: () => void;
  loginWithGithub?: () => void;
  loginWithTwitter?: () => void;
  confirm: (code: string, username: string) => Promise<unknown>;
};

export type ApplicationContextType =
  | {
      isInitialized: false;
      isAuthenticated: false;
    }
  | {
      isInitialized: true;
      isAuthenticated: false;
    }
  | {
      isInitialized: true;
      isAuthenticated: true;
      id: string;
    };

export type ProfileRequestResponse = { id: string };
