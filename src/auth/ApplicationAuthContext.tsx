import { createContext, useEffect, useState } from 'react';
import { Maybe } from 'yup';
import { ApplicationContextType, ProfileRequestResponse } from './types';
import { useAuthContext } from './useAuthContext';
import axiosInstance from '../utils/axios';

export const ApplicationAuthContext = createContext<Maybe<ApplicationContextType>>(null);

type ApplicationAuthContextProps = {
  children: React.ReactNode;
};

const DEFAULT_AUTH_STATE: ApplicationContextType = {
  isInitialized: false,
  isAuthenticated: false,
};

const useApplicationAuthState = () => {
  const [state, setState] = useState<ApplicationContextType>(DEFAULT_AUTH_STATE);

  const logout = () => setState({ isInitialized: true, isAuthenticated: false });

  const login = (id: string) => setState({ isInitialized: true, isAuthenticated: true, id });

  const isLoggedOut = state.isInitialized && !state.isAuthenticated;
  const isLoggedIn = state.isInitialized && state.isAuthenticated;

  return { state, logout, login, isLoggedOut, isLoggedIn };
};

export const ApplicationAuthProvider = ({ children }: ApplicationAuthContextProps) => {
  const state = useApplicationAuthState();
  const cognitoSession = useAuthContext();

  useEffect(() => {
    if (!cognitoSession.isInitialized) {
      return;
    }
    if (!cognitoSession.isAuthenticated && !state.state.isInitialized) {
      state.logout();
      return;
    }

    if (state.state.isInitialized) {
      return;
    }

    const token = cognitoSession?.user?.cognitoUser
      ?.getSignInUserSession()
      ?.getIdToken()
      ?.getJwtToken();

    getCurrentUser(token ?? 'NO_TOKEN')
      .then((user) => {
        if (!state.isLoggedIn) {
          state.login(user.id);
        }
      })
      .catch(() => {
        state.logout();
      });
  }, [
    cognitoSession.isInitialized,
    cognitoSession.isAuthenticated,
    state,
    cognitoSession?.user?.cognitoUser,
  ]);

  return (
    <ApplicationAuthContext.Provider value={state.state}>
      {children}
    </ApplicationAuthContext.Provider>
  );
};

const getCurrentUser = async (token: string): Promise<ProfileRequestResponse> => {
  try {
    const result = await axiosInstance.get<ProfileRequestResponse>('/api/auth/profile', {
      withCredentials: true,
    });
    return result.data;
  } catch (err) {
    const response = await axiosInstance.post<ProfileRequestResponse>(
      '/api/auth/login',
      undefined,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    return response.data;
  }
};
