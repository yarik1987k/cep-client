import { useContext } from 'react';
import { AuthContext } from './AwsCognitoContext';
import { ApplicationAuthContext } from './ApplicationAuthContext';

export const useApplicationAuthContext = () => {
  const context = useContext(ApplicationAuthContext);

  if (!context)
    throw new Error('useApplicationAuthContext context must be use inside ApplicationAuthProvider');

  return context;
};
