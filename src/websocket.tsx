import { createContext, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { Maybe } from 'yup';
import { WebsocketListener, WebsocketMessage } from './types';

const RECONNECT_TIMEOUT_MS = 3000;

type WebsocketProviderProps = {
  children: React.ReactNode;
};

interface IWebsocketContext {
  sendMessage: (message: unknown) => void;
  addTypeListener: (type: string, listener: WebsocketListener) => void;
  removeTypeListener: (type: string, listener: WebsocketListener) => void;
  addRidListener: (rid: string, listener: WebsocketListener) => void;
  removeRidListener: (rid: string, listener: WebsocketListener) => void;
}

export const WebsocketContext = createContext<IWebsocketContext | null>(null);

// type Listener = (message: WebsocketMessage) => void;
// type Listeners = { [key in string]: Listener[] };

const useAutoConnectWebsocket = (host: string, onMessage: (message: WebsocketMessage) => void) => {
  const [socket, setSocket] = useState<Maybe<WebSocket>>(null);

  const createSocket = useCallback(() => {
    const client = new WebSocket(host);

    client.onopen = (event) => {
      console.log('WS open', event);
      setSocket(client);
    };

    client.onmessage = (event) => {
      try {
        const data = JSON.parse(event.data);
        onMessage(data);
      } catch (e) {
        console.error('WS onmessage error', event, e);
      }
    };

    client.onclose = (event) => {
      console.log('WS close', event);
      setSocket(null);
      setTimeout(() => setSocket(createSocket()), RECONNECT_TIMEOUT_MS);
    };

    client.onerror = (event) => {
      console.log('WS error', event);
      // setSocket(null);
      // setTimeout(() => setSocket(createSocket), RECONNECT_TIMEOUT_MS);
    };

    return client;
  }, [host, onMessage]);

  useEffect(() => {
    console.log('Effect');
    const client = createSocket();
    setSocket(client);
  }, [createSocket]);

  return socket;
};

const useListeners = () => {
  const listenersRef = useRef(new Map<string, WebsocketListener[]>());
  const listeners = listenersRef.current;

  const addListener = useCallback(
    (key: string, listener: WebsocketListener) => {
      if (!listeners.has(key)) {
        listeners.set(key, []);
      }
      listeners.get(key)?.push(listener);
    },
    [listeners]
  );

  const removeListener = useCallback(
    (key: string, listener: WebsocketListener) => {
      const filtered = (listeners.get(key) || []).filter((it) => it !== listener);
      if (filtered.length > 0) {
        listeners.set(key, filtered);
      }
      if (filtered.length === 0 && listeners.has(key)) {
        listeners.delete(key);
      }
    },
    [listeners]
  );

  const apply = useCallback(
    (key: string, message: WebsocketMessage) => {
      // console.log('key, listeners.get(key)', key, listeners.get(key));
      listeners.get(key)?.forEach((listener) => listener(message));
    },
    [listeners]
  );

  const memoized = useMemo(
    () => ({ apply, addListener, removeListener }),
    [addListener, apply, removeListener]
  );

  return memoized;
};

console.log('location', {
  host: window.location.host,
  hostname: window.location.hostname,
  protocol: window.location.protocol,
});

const socketProtocol = (protocol: string): 'wss' | 'ws' => {
  if (protocol.toLowerCase().startsWith('https')) {
    return 'wss';
  }
  return 'ws';
};

const serverUrl = `${socketProtocol(window.location.protocol)}://${window.location.host}/api/ws`;

export const WebsocketProvider = ({ children }: WebsocketProviderProps) => {
  // const { user } = useAuthContext();
  // const clientRef = useRef<WebSocket>();

  const typeListeners = useListeners();
  const ridListeners = useListeners();

  const onMessage = useCallback(
    (message: WebsocketMessage) => {
      console.log('WS receive message', message);
      if (message.rid) {
        ridListeners.apply(message.rid, message);
      }
      typeListeners.apply(message.type, message);
    },
    [ridListeners, typeListeners]
  );

  const socket = useAutoConnectWebsocket(serverUrl, onMessage);

  // const listenersRef = useRef<Listeners>({});

  const sendMessage = useCallback(
    (message: unknown) => {
      // console.log(
      //   'Try to send message',
      //   message,
      //   socket?.readyState === socket?.OPEN
      // );
      console.log('WS send message:', message);
      socket?.send(JSON.stringify(message));
    },
    [socket]
  );

  const addTypeListener = useCallback(
    (type: string, listener: WebsocketListener) => {
      typeListeners.addListener(type, listener);
    },
    [typeListeners]
  );

  const removeTypeListener = useCallback(
    (type: string, listener: WebsocketListener) => {
      typeListeners.removeListener(type, listener);
    },
    [typeListeners]
  );

  const addRidListener = useCallback(
    (rid: string, listener: WebsocketListener) => {
      ridListeners.addListener(rid, listener);
    },
    [ridListeners]
  );

  const removeRidListener = useCallback(
    (rid: string, listener: WebsocketListener) => ridListeners.removeListener(rid, listener),
    [ridListeners]
  );

  // const addListener = (type: string, listener: Listener) x=> {
  //   if (!listenersRef.current[type]) {
  //     listenersRef.current[type] = [];
  //   }
  //   listenersRef.current[type].push(listener);
  // };
  //
  // const removeListener = (type: string, listener: Listener) => {
  //   listenersRef.current[type] = (listenersRef.current[type] || []).filter((it) => it !== listener);
  // };

  const memoized = useMemo(
    () => ({
      sendMessage,
      addTypeListener,
      removeTypeListener,
      addRidListener,
      removeRidListener,
    }),
    [addRidListener, addTypeListener, removeRidListener, removeTypeListener, sendMessage]
  );

  return <WebsocketContext.Provider value={memoized}>{children}</WebsocketContext.Provider>;
};
