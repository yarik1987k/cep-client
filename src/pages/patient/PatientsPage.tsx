import { Helmet } from 'react-helmet-async';
import { Box, Button, Container, Typography, Table, TableBody } from '@mui/material';
import { useCallback, useEffect } from 'react';
import { useQuery } from 'react-query';
import { alpha } from '@mui/material/styles';
import { Link as RouterLink } from 'react-router-dom';
import { useUploadFileMutation } from '../../service/file';
import { Upload } from '../../components/upload';
import { useWebsocketRequest } from '../../hooks/useWebsocketRequest';
import { useGetPatientsQuery } from '../../service/patient';
import { useSettingsContext } from '../../components/settings';
import { TableHeadCustom, useTable } from '../../components/table';
import PatientTableRow from '../../sections/@dashboard/patient/list/PatientTableRow';
import CustomBreadcrumbs from '../../components/custom-breadcrumbs';
import { PATH_DASHBOARD } from '../../routes/paths';
import Iconify from '../../components/iconify';

// const useGetArticlesRequest = () => {
//   const request = useWebsocketRequest('ceph-list-response');
//   return (): Promise<{ cases: string[] }> =>
//     request({
//       type: 'ceph-list-request',
//     });
// };

// const useGetArticles = () => {
//   const request = useGetArticlesRequest();
//   return useQuery<string[]>({
//     queryKey: 'ceph-list-response',
//     queryFn: () => request().then((it) => it.cases),
//   });
// };

const TABLE_HEAD = [
  { id: 'firstName', label: 'First Name', align: 'left' },
  { id: 'lastName', label: 'Last name', align: 'left' },
  { id: '' },
];

export default function PatientsPage() {
  // const articlesRequest = useGetArticles();
  //
  // const mutation = useUploadFileMutation();

  // const handleDropSingleFile = useCallback(
  //   (acceptedFiles: File[]) => {
  //     const file = acceptedFiles[0];
  //
  //     if (file) {
  //       mutation.mutate(file);
  //       // setFile(
  //       //     Object.assign(file, {
  //       //         preview: URL.createObjectURL(file),
  //       //     })
  //       // );
  //     }
  //   },
  //   [mutation]
  // );

  const { themeStretch } = useSettingsContext();

  const patientsQuery = useGetPatientsQuery();

  const patients = patientsQuery.data;

  console.log('patients', patients);

  const {
    dense,
    page,
    order,
    orderBy,
    rowsPerPage,
    setPage,
    //
    selected,
    setSelected,
    onSelectRow,
    onSelectAllRows,
    //
    onSort,
    onChangeDense,
    onChangePage,
    onChangeRowsPerPage,
  } = useTable();

  return (
    <>
      <Helmet>
        <title> General: App | Minimal UI</title>
      </Helmet>

      <Container maxWidth={themeStretch ? false : 'xl'}>
        <CustomBreadcrumbs
          heading="Patients"
          links={[{ name: 'Dashboard', href: PATH_DASHBOARD.root }, { name: 'Patients' }]}
          action={
            <Button
              component={RouterLink}
              to={PATH_DASHBOARD.patient.new}
              variant="contained"
              startIcon={<Iconify icon="eva:plus-fill" />}
            >
              New Patient
            </Button>
          }
        />

        {patients && (
          <Table size={dense ? 'small' : 'medium'} sx={{ minWidth: 800 }}>
            <TableHeadCustom
              order={order}
              orderBy={orderBy}
              headLabel={TABLE_HEAD}
              rowCount={patients.length}
              numSelected={selected.length}
              onSort={onSort}
              onSelectAllRows={(checked) =>
                onSelectAllRows(
                  checked,
                  patients.map((row) => row.id)
                )
              }
            />
            <TableBody>
              {patients.map((patient) => (
                <PatientTableRow
                  key={patient.id}
                  patient={patient}
                  selected={selected.includes(patient.id)}
                  onDeleteRow={() => {}}
                  onEditRow={() => {}}
                  onSelectRow={() => {}}
                  onViewRow={() => {}}
                />
              ))}
            </TableBody>
          </Table>
        )}
      </Container>

      {/* <Upload onDrop={handleDropSingleFile} /> */}
    </>
  );
}
