import { Helmet } from 'react-helmet-async';
import { Container } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { useSettingsContext } from '../../components/settings';
import { PATH_DASHBOARD } from '../../routes/paths';
import CustomBreadcrumbs from '../../components/custom-breadcrumbs';
import { PatientNewEditForm } from '../../sections/@dashboard/patient/PatientNewEditForm';
import { useCreatePatientMutation } from '../../service/patient';

export default function PatientCreatePage() {
  const { themeStretch } = useSettingsContext();

  const navigate = useNavigate();

  const mutation = useCreatePatientMutation({
    onSuccess: (data) => {
      console.log('onSuccess() data', data);
      navigate(PATH_DASHBOARD.patient.patient(data.id));
    },
  });

  return (
    <>
      <Helmet>
        <title> Patient: Create a new patient</title>
      </Helmet>
      <Container maxWidth={themeStretch ? false : 'lg'}>
        <CustomBreadcrumbs
          heading="Create a new patient"
          links={[
            {
              name: 'Dashboard',
              href: PATH_DASHBOARD.root,
            },
            {
              name: 'Patients',
              href: PATH_DASHBOARD.patient.list,
            },
            // {
            //     name: 'User',
            //     href: PATH_DASHBOARD.user.list,
            // },
            { name: 'New patient' },
          ]}
        />
        <PatientNewEditForm submit={mutation.mutateAsync} />
      </Container>
    </>
  );
}
