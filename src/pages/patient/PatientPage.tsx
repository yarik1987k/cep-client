import { Helmet } from 'react-helmet-async';
import { useParams } from 'react-router-dom';
import { Container } from '@mui/material';
import { useGetPatientQuery } from '../../service/patient';
import Page404 from '../Page404';
import { patientFullName } from '../../utils/patient';
import { PATH_DASHBOARD } from '../../routes/paths';
import CustomBreadcrumbs from '../../components/custom-breadcrumbs';
import { useSettingsContext } from '../../components/settings';
import { PatientDetails } from '../../sections/@dashboard/patient/details/PatientDetails';

export default function PatientPage() {
  const { id } = useParams();
  const { themeStretch } = useSettingsContext();

  const { data: patient, isLoading } = useGetPatientQuery(id || '', {
    enabled: !!id,
  });

  console.log('patient', patient);

  // TODO redirect to 404 if no patient found

  if (!patient && !isLoading) {
    return <Page404 />;
  }

  if (!patient) {
    // TODO handle loading
    return null;
  }

  const fullName = patientFullName(patient);

  return (
    <>
      <Helmet>
        <title> Patient: {fullName}</title>
      </Helmet>

      <Container maxWidth={themeStretch ? false : 'lg'}>
        <CustomBreadcrumbs
          heading="Profile"
          links={[
            { name: 'Dashboard', href: PATH_DASHBOARD.root },
            { name: 'Patients', href: PATH_DASHBOARD.patient.list },
            { name: fullName },
          ]}
        />
        <PatientDetails patient={patient} />
      </Container>
    </>
  );
}
