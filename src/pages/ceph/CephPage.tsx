import { Helmet } from 'react-helmet-async';
import { useParams } from 'react-router-dom';
import { Box, Button, Container, Grid, LinearProgress } from '@mui/material';
import { useMemo, useState } from 'react';
import { Maybe } from 'yup';
import { useQueryClient } from 'react-query';
import { LoadingButton } from '@mui/lab';
import { useGetCephQuery, useUpdateCeph } from '../../service/ceph';
import { useGetFileQuery } from '../../service/file';
import { CephViewer } from '../../sections/@dashboard/ceph/ceph-viewer/CephViewer';
import { useSettingsContext } from '../../components/settings';
import { PATH_DASHBOARD } from '../../routes/paths';
import CustomBreadcrumbs from '../../components/custom-breadcrumbs';
import {
  CoordinatesPatch,
  PatchedCoordinate,
} from '../../sections/@dashboard/ceph/ceph-viewer/types';
import { Ceph } from '../../service/types';
import { shortUUID } from '../../utils/patient';
import { SeoIllustration } from '../../assets/illustrations';
import { AppWelcome } from '../../sections/@dashboard/__old/general/app';

// TODO listen for event of processing

export default function CephPage() {
  const queryClient = useQueryClient();

  const { id } = useParams();
  const { themeStretch } = useSettingsContext();

  const [patch, setPatch] = useState<Maybe<CoordinatesPatch>>(null);

  const cephRequest = useGetCephQuery(id || '', {
    enabled: !!id,
  });

  const updateMutation = useUpdateCeph({
    onSuccess: (ceph) => {
      setPatch(null);
      queryClient.setQueryData(['ceph', id], ceph);
    },
  });

  const fileId = cephRequest.data?.cephFile;
  const processingResult = cephRequest.data?.result;
  const urlRequest = useGetFileQuery(fileId || '', {
    enabled: !!fileId,
  });

  const loading = cephRequest.isLoading || urlRequest.isLoading;

  const url = urlRequest.data?.url;

  const patchedCoordinates: Maybe<PatchedCoordinate[]> = useMemo(
    () =>
      processingResult?.coordinates?.map((coordinate, index) => {
        const patched = (patch ?? {})[index];
        if (patched) {
          return { ...patched, patch: true };
        }
        return coordinate;
      }),
    [processingResult?.coordinates, patch]
  );

  const onChange = (change: { index: number; x: number; y: number }) => {
    setPatch((prevPatch) => ({
      ...(prevPatch || {}),
      [change.index]: { x: change.x, y: change.y, c: 1 },
    }));
  };

  const onSaveHandle = () => {
    const ceph = cephRequest.data;
    if (!ceph || !ceph.result) {
      return;
    }
    const update: Ceph = {
      ...ceph,
      result: {
        ...ceph.result,
        coordinates: ceph.result.coordinates.map(
          (coordinate, index) => (patch && patch[index]) || coordinate
        ),
      },
    };
    updateMutation.mutate(update);
  };

  return (
    <>
      <Helmet>
        <title> Ceph </title>
      </Helmet>

      <Container maxWidth={themeStretch ? false : 'lg'}>
        <CustomBreadcrumbs
          heading="Ceph"
          links={
            fileId
              ? [
                  { name: 'Dashboard', href: PATH_DASHBOARD.root },
                  {
                    name: 'Patient',
                    href: PATH_DASHBOARD.patient.patient(cephRequest.data?.patient || ''),
                  },
                  { name: `${shortUUID(fileId)}` },
                ]
              : [
                  { name: 'Dashboard', href: PATH_DASHBOARD.root },
                  {
                    name: 'Patient',
                    href: PATH_DASHBOARD.patient.patient(cephRequest.data?.patient || ''),
                  },
                ]
          }
        />

        {loading && <LinearProgress color="primary" />}

        {!cephRequest.data?.result && (
          <AppWelcome
            title="Ceph is not processed yet"
            description="Ceph is already under processing, you just need to wait a little bit. It's also possible that processing failed, and you will never get the result..."
            img={
              <SeoIllustration
                sx={{
                  p: 3,
                  width: 360,
                  margin: { xs: 'auto', md: 'inherit' },
                }}
              />
            }
            action={<Button variant="contained">Click for nothing</Button>}
          />
        )}

        {url && patchedCoordinates && processingResult && (
          <Grid container spacing={3}>
            <Grid item lg={10} md={9} xs={12}>
              <CephViewer
                url={url}
                imageSize={processingResult.imageSize}
                coordinates={patchedCoordinates}
                onChange={onChange}
              />
            </Grid>
            <Grid item lg={2} md={3} xs={12}>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <LoadingButton
                    fullWidth
                    variant="contained"
                    onClick={onSaveHandle}
                    loading={updateMutation.isLoading}
                    disabled={!patch}
                  >
                    Save
                  </LoadingButton>
                </Grid>
                {patch && !updateMutation.isLoading && (
                  <Grid item xs={12}>
                    <Button variant="contained" fullWidth onClick={() => setPatch(null)}>
                      Reset
                    </Button>
                  </Grid>
                )}
              </Grid>
            </Grid>
          </Grid>
        )}
      </Container>
    </>
  );
}
