const { createProxyMiddleware } = require('http-proxy-middleware');

const host = process.env.SERVER || 'http://localhost:8080';

module.exports = function (app) {
  app.use(
    createProxyMiddleware('/api', {
      target: host,
      changeOrigin: true,
    })
  );

  app.use(
    createProxyMiddleware('/api/ws', {
      target: socketHost(host),
      ws: true,
      changeOrigin: true,
    })
  );
};

const socketHost = (host) => {
  const delimiter = '://';
  const parts = host.split(delimiter);
  return [socketProtocol(parts[0]), delimiter, parts[1]].join('');
};

const socketProtocol = (protocol) => {
  if (protocol.toLowerCase().startsWith('https')) {
    return 'wss';
  }
  return 'ws';
};
