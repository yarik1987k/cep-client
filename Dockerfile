FROM node:16.17.1-alpine3.16 as build

ARG REACT_APP_AWS_COGNITO_USER_POOL_ID
ARG REACT_APP_AWS_COGNITO_CLIENT_ID
ARG REACT_APP_AWS_COGNITO_REGION
ARG REACT_APP_AWS_COGNITO_DOMAIN
ARG REACT_APP_AWS_GOGNITO_APPLICATION_URL

ENV GENERATE_SOURCEMAP=false

WORKDIR /usr/app

COPY package.json package-lock.json ./
RUN npm ci

COPY src src
COPY public public
COPY tsconfig.json .eslintrc .eslintignore .prettierrc ./

RUN npm run build

FROM nginx:1.23.1-alpine
EXPOSE 80
COPY ./docker/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf
COPY --from=build /usr/app/build /usr/share/nginx/html
